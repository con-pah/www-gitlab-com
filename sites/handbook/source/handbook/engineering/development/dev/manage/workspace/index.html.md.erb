---
layout: handbook-page-toc
title: "Workspace Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Manage:Workspace
{: #welcome}


### How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.
* We do an optional, asynchronous daily stand-up in our respective group stand-up channels:
  * Manage:Workspace [#g_manage_workspace_daily](https://gitlab.slack.com/archives/C02J4GTP38A)


#### Capacity Planning & Estimation

Prior to release kickoff, the Engineering Manager needs to provide the total teams capacity in weight to the Product Manager. Each engineer on the team starts at a baseline weight of capability and is further reduced in capacity based on planned time off including holidays, company events such as [Family and Friends day](/company/family-and-friends-day/) or a team day, on-call shifts, and so on. The end result is a weight estimate that each engineer is reasonably capable of accomplishing, added up amongst all ~"frontend" engineers and ~"backend" engineers separately, and added to the planning issue.

In order to more accurately understand what work can be planned for a release without exhausting our team members, we provide weight estimations after a preliminary, surface-level investigation. We recognize that estimations may not be precise! However this is key in ensuring that the capacity planning estimate above is not overwhelming to the team.

When estimating development work, please assign an issue the appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests affected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. We should challenge ourselves to break this issue in to smaller pieces. |

We do not provide estimates greater than 5 without encouraging iteration, splitting the issue, collaborating on what a lower effort MVC might be, or working with our counterparts to understand the unknowns. 

#### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline). While meeting this timeline is flexible, a typical planning cycle is suggested to look like:

* At the beginning of each month, Product should have created [a planning issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues?sort=created_date&state=opened&label_name[]=group::workspace&label_name[]=Planning+Issue) for the coming release.
  * This issue should include an initial plan for the release, along with links to boards that represent the proposed work for the milestone.
  * Issues without estimates should be investigated by engineering throughout the month so weighting does not need to happen at once. Issues of particular significance to our stage's strategy should be marked with `direction`.
* By the 12th, all planned issues proposed for the next release should be estimated by engineering (`workflow:ready for development`).
  * The EM should add any issues that have the potential to slip and their new weight estimates. Issues that we are sure will slip should be automatically rescheduled to the upcoming release in advance.
  * After estimation, Product should make any needed adjustments to the proposed scope based on estimates. A synchronous meeting to review the final release scope is recommended. At this stage, any issues that the team is committing to delivering should have the ~Deliverable label applied to them.
  * The EM will then assign those issues to individuals based on their individual capacity, and the release will begin

##### What issues get planned?

Between when the planning issue is created and when it gets finalized, [everyone can contribute](/company/mission/#everyone-can-contribute). [All group members](#group-members) can propose issues to be considered for the upcoming release here. Examples of some things to consider when proposing items would be:
- Issues you have been trying to get prioritized or make time for in your free time
- Performance, Application Limit, Reliability, Security, or Infradev issues
- Bugs that you'd like to see resolved or are starting to pick up in frustration
- Issues that may have been tentatively scheduled to this release already, for example when a security issue gets scheduled to a future milestone

#### During a release

 In general, engineering will follow this workflow label cycle for any ~Deliverable issues in the release:

1. `workflow::ready for development` - work is ready to begin, but has not yet started
1. `workflow::in dev` - we are looking at the issue, and are either actively investigating, have began development, or have a draft merge request open
1. `workflow::in review` - a merge request has been submitted and reviews have been requested
1. `workflow::verification` - the work has merged, and needs to be verified by the assignee

### Working on unscheduled issues

With the combination of our capacity planning (EM) and estimation (IC) processes above, engineers should have free time to work on ~"Stuff that should just work" or other topics that interest them. If an unscheduled issue should really be prioritized, bring it up in a planning issue or ask your manager to reduce your capacity further.

### Additional considerations

<%= partial("handbook/engineering/development/dev/manage/workspace_processes.erb") %>
<%= partial("handbook/engineering/development/dev/manage/workspace/okr_training_process.erb") %>

## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Weekly    | Group-level meeting                  | Backend Engineering Managers | Ensure current release is on track by walking the board, unblock specific issues                       |
| Monthly   | Planning meetings                    | Product Managers         | See [Planning](/handbook/engineering/development/dev/manage/#planning) section |

For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Agenda documents and recordings can be placed in the [shared Google drive](https://drive.google.com/drive/u/0/folders/0ALpc3GhrDkKwUk9PVA) (internal only) as a single source of truth.

Meetings that are not 1:1s or covering confidential topics should be added to the Manage Shared calendar.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.


## Group Members

The following people are permanent members of the group:

<%= stable_counterparts(role_regexp: /Manage.+Workspace/) %>

## Dashboards

- [Performance Indicators](https://app.periscopedata.com/app/gitlab/932405/Manage::Workspace-Dashboard)


## Links and resources
{: #links}

* Error budgets (internal only)
  * [Kibana](https://log.gprd.gitlab.net/goto/f5d819e0-8834-11ec-9dd2-93d354bef8e7) - shows the endpoints which contribute to budget spend
  * [Grafana](https://dashboards.gitlab.net/d/stage-groups-workspace/stage-groups-group-dashboard-manage-workspace?orgId=1)
  * [Sisense](https://app.periscopedata.com/app/gitlab/892223/Error-Budget-Dashboard---Stage-Manage) - shows historical data
<%= partial("handbook/engineering/development/dev/manage/shared_links.erb") %>
* [Milestone retrospectives](https://gitlab.com/gl-retrospectives/manage-stage/workspace/-/issues)
* Our Slack channels
  * Manage:Workspace [#g_manage_workspace](https://gitlab.slack.com/archives/C02AZ7C32N5)
  * Linear Queries [#g_manage_auth_topic_linear_namespaces_queries](https://gitlab.slack.com/archives/C02C507V2PM)
  * Consolidating groups and projects [#f_simplify-groups-and-projects](https://gitlab.slack.com/archives/C014ZJZP0JC)
  * Daily standups [#g_manage_workspace_daily](https://gitlab.slack.com/archives/C02J4GTP38A)
