---
layout: handbook-page-toc
title: LifeLabs Learning Training for Managers at GitLab
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is LifeLabs Learning

Read more about [LifeLabs Learning](https://lifelabslearning.com/) on their website.

### Get involved:

1. Join the [#lifelabs-learning Slack channel](https://app.slack.com/client/T02592416/C036Y740SKY/thread/C5P8T9VQX-1587584276.009700)
2. Add yourself to the waitlist or indicate interest in a future cohort using [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/53)

## FY23 Manager Core 1

40 managers will complete the LifeLabs Manager Core 1 from May 9th to September 26th. Managers are split into 4 cohorts of 10 managers each and will meet for a total of 5 training sessions. 


## FY22/FY23 Pilot Program

LifeLabs piloted their [Manager Core 1](https://drive.google.com/file/d/1MJmxjrMSSCq3lWOOks-vMnzdzPucI0jp/view) program to a group of ten managers in the early months of 2022.

Below is the schedule from the pilot program: 

| Workshop | Date | Time | 
| ------ | ------ | ------ |
| Coaching Skills | Thursday 2022-01-13 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Feedback Skills | Thursday 2022-01-27 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Productivity & Prioritization | Thursday 2022-02-17 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Effective 1:1s | Wednesday 2022-02-23 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |
| Manager Intensive 1 | Thursday 2022-05-26 | 8am-10am PST / 11am-1pm EST / 4pm-6pm GMT |

### Pilot feedback and results

1. Key reflections and resources can be found in [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/365)


